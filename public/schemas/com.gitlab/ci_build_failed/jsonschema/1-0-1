{
  "$schema": "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
  "description": "Schema for a CI Build Failed event",
  "self": {
    "vendor": "com.gitlab",
    "name": "ci_build_failed",
    "version": "1-0-1",
    "format": "jsonschema"
  },
  "type": "object",
  "required": [
    "project",
    "build_id",
    "build_name",
    "exit_code",
    "failure_reason"
  ],
  "properties": {
    "project": {
      "description": "ID of the project that the scan ran on",
      "type": "integer",
      "minimum": 0,
      "maximum": 2147483647
    },
    "build_id": {
      "description": "ID of the CI build that failed",
      "type": "integer",
      "minimum": 0,
      "maximum": 9223372036854775807
    },
    "build_name": {
      "description": "Name of the build (job name) that failed",
      "type": "string",
      "maxLength": 65535
    },
    "build_artifact_types": {
      "description": "List of the artifact types that were produced by the build",
      "type": ["array", "null"],
      "items": {
        "description": "The type of the build artifact that was produced, e.g. `sast` or `dependency_scanning`",
        "type": "string",
        "maxLength": 65535
      }
    },
    "exit_code": {
      "description": "The exit code produced by the failing CI build",
      "type": "integer",
      "minimum": 0,
      "maximum": 2147483647
    },
    "failure_reason": {
      "description": "Reason for the job failure",
      "type": "string",
      "maxLength": 65535
    }
  }
}
